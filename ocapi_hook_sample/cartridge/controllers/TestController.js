/**
 * Get the active site currencies and prepare a response in JSON format.
 */
exports.TestJSON = function () {
    var currencies = require('~/cartridge/scripts/currencies');
    // get the active site currencies as comma separated string
    var siteCurrencies = currencies.getSiteCurrencies();

    // prepare the response
    response.setContentType('application/json');
    response.writer.print(JSON.stringify({
        site_currencies : siteCurrencies
    }));
};

exports.TestJSON.public = true;
