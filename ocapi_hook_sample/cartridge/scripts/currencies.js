/**
 * Return the active site currencies as array.
 * 
 * @returns {Array} the site currencies
 */
function getSiteCurrencies() {
    return require('dw/system/Site').getCurrent().getAllowedCurrencies().toArray();
}

exports.getSiteCurrencies = getSiteCurrencies;
