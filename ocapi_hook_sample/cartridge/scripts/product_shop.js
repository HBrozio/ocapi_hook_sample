var Status = require('dw/system/Status');

exports.beforeGET = function (productId) {
    // switch the session currency to EUR, if the product id is equal to 25752218
    if (productId === '25752218') {
        session.setCurrency(require('dw/util/Currency').getCurrency('EUR'));
    }
    return new Status(Status.OK);
};

exports.modifyGETResponse = function (scriptProduct, productWO) {
    var currencies = require('~/cartridge/scripts/currencies');
    // get the active site currencies as array
    var siteCurrencies = currencies.getSiteCurrencies();

    // set active site currencies as none persistent
    // custom attribute at the OCAPI Shop API products response
    productWO.c_site_currencies = siteCurrencies;
    return new Status(Status.OK);
};
