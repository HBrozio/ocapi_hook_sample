What this cartridge does?

This is a sample cartridge on base of the SiteGenesis reference application, which shows how extension points (aka hooks) 
are used with the OCAPI Shop API to customize an OCAPI products resource. 
Additionally a controller script is provided, which shares a script function with the OCAPI hook script.

How to install this cartridge?

Please perform the following steps to install this cartridge:

- Upload the cartridge via WebDAV to the active cartridge directory of your Sandbox instance for example to https://<host>/on/demandware.servlet/webdav/Sites/Cartridges/version1.
- In the Business Manager add the cartridge to your cartridge list of SiteGenesis via Administration > Manage Sites > SiteGenesis > Settings and add it to Cartridges for example "ocapi_hook_sample:sitegenesis_storefront_controllers:sitegenesis_storefront_core".

How to execute the example?

To call the controller script example, you just need to call the following URL via a browser:
http://<host>/on/demandware.store/Sites-SiteGenesis-Site/default/TestController-TestJSON

To execute the customized OCAPI Shop API products resource, you can call the URL
http://<host>/s/SiteGenesis/dw/shop/v17_8/products/25752218?client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa&expand=prices

What does the code do?

The controller script example:

By calling the controller URL the script TestController and the method TestJSON is called. This method uses the shared file currencies.js and invokes the method getSiteCurrencies() in there.
The method return an array of all active site currencies. At the response of the controller call the content type as well as the prepared JSON result is set.

The OCAPI Shop API extension point example:

In the cartridge the package.json file tells the cartridge logic where the hooks.json file is located.
This hooks.json file registeres the script file product_shop.js for the OCAPI Shop API extension point beforeGET and modifyGETResponse of the Shop API products resource
(see https://documentation.demandware.com/DOC1/index.jsp?topic=%2Fcom.demandware.dochelp%2FOCAPI%2F17.8%2Fshop%2FResources%2FProducts.html&cp=0_12_3_13 below Customization).

If the OCAPI Shop API products resource is now called via the URL, the beforeGET hook is executed. The code can be found in the products_shop.js file in the beforeGET function. The function sets the session currency to EUR, if the current product id is 25752218.
After that the internal OCAPI Shop API products resource logic is executed. Before the OCAPI response is returned, the modifyGETResponse function is called. This function again retrieves the active site currencies and sets them as a none persistent custom attribute to the OCAPI products response.


